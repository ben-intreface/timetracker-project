<?php

use \Bitrix\Main\{
    Event,
    UserTable
};

use \Intreface\Helper\{
    Patcher\Component,
    Patcher\Placement
};

use \Intreface\Module\{
    TimeTracker\Settings
};

Component::getInstance()->addResultModifier('bitrix:menu', 'left_vertical', Placement::ON_AFTER, function (Event $event) {
    $arResult = $event->getParameter('arResult');
    global $USER;
    if(\in_array(Settings::PERMISSIONS_USER_GROUP, UserTable::getUserGroupIds($USER->GetID()))) {
        $arResult["ITEMS"]['show'][] = [
            "TEXT" => 'Time Tracker',
            "LINK" => '/timetracker/tracker.php',
            "PERMISSION" => "X",
            "CHAIN" => ['TIMETRACKER']
        ];
    }
    $event->setParameter('arResult', $arResult);
});