<?php

declare(strict_types=1);

use \Bitrix\Main\{
    EventManager,
    Application
};

use \Intreface\Module\{
    TimeTracker\Settings
};

/*
 * Author Ben Berhanu
 * With contribution from Mikkie
 */
EventManager::getInstance()->addEventHandler(
    "main",
    'OnProlog',
    function () {

        $request = Application::getInstance()->getContext()->getRequest();

        global $APPLICATION, $USER;

        // if on Tasks pages and User is in the permissions group
        if (strpos((string)$request->getRequestedPage(), '/tasks/') && !strpos((string)$request->getRequestedPage(), '/view') && !strpos((string)$request->getRequestedPage(), '/edit') && \in_array(Settings::PERMISSIONS_USER_GROUP, $USER->GetUserGroupArray())) {

            $APPLICATION->AddViewContent("topblock", '<div id="timeTracker"></div>');
            $APPLICATION->SetAdditionalCSS("/local/assets/package/intreface.helper-ui/js/quasar/quasar.css", true);
            $APPLICATION->SetAdditionalCSS("/local/timetracker/css/style.css", true);
            $APPLICATION->AddHeadString('<script type ="module" src="/local/timetracker/js/script.js?' . time() . '"></script>');

        }
    }
);
