<?php


namespace Intreface\Module\TimeTracker;


/**
 * Class Settings
 * @package Intreface\Module\TimeTracker
 * @todo need to extend this and create new table to store these values. Will need to create an interface in the admin control panel to input and edit these values
 */
class Settings
{
    protected const MODULE_ID = 'intreface.module_timetracker';
    /**
     * Development values commented out
     */
//    const PERMISSIONS_USER_GROUP = 1;
//    const INTERNAL_GROUP_ID = 1;
//    const INTERNAL_EMAIL_DOMAIN = "intreface.com";
//    const INTERNAL_EMAIL_PLACEHOLDER = "internal@".self::INTERNAL_EMAIL_DOMAIN;
//    const SUPPORT_TASK_RESPONSIBLE = 1;
//    const SUPPORT_TASK_PARTICIPANTS = 1;

    /**
     * Production values
     * @const PERMISSIONS_USER_GROUP int => user's permission
     * @const INTERNAL_GROUP_ID Workgroup for internal tasks
     */
    const PERMISSIONS_USER_GROUP = 33;
    const INTERNAL_GROUP_ID = 74;
    const INTERNAL_EMAIL_DOMAIN = "intreface.com";
    const INTERNAL_EMAIL_PLACEHOLDER = "internal@".self::INTERNAL_EMAIL_DOMAIN;
    const SUPPORT_TASK_RESPONSIBLE = 14;
    const SUPPORT_TASK_PARTICIPANTS = 1912;

}