<?php


namespace Intreface\Module\TimeTracker;

use CTicket;
use Bitrix\Main\{
    Loader,
    UserTable,
    UserFieldTable
};
use Intreface\Module\TimeTracker\{
    Tasks,
    Settings
};

/**
 * Class Tickets
 * @package Intreface\Module\TimeTracker
 */

class Tickets extends Action
{

    // need to fix the ticket/message confusion To get the ticket ID from the ticket message I need to use TICKET_ID
    // Not ID, this would be the message ID. I should use ticket ID here.

    /**
     * @param int $ticketId
     * @return int
     * @throws \Exception
     */
    public static function resolveTicketTask(int $ticketId): int{
        //get ticket
        $ticket = self::getTicket($ticketId);

        //get ticket author
        $user = self::getUserFromTicket($ticket);

        //get author group
        $project = self::getUserProject($user);

        if (empty($project)) {
            return 0;
        }

        //get annual support task from group or crete one of it does not exist and return value task ID
        $taskId = self::getSupportTask($project);

        if (!is_int($taskId)) {
            throw new \Exception('Support task not found [02032021.11111.11111]', 500);
        }
        return $taskId;

    }

    /**
     * @param int $ticketId
     * @return string
     */
    public static function getTicketName(int $ticketId): string
    {
        $ticket = self::getTicket($ticketId);
        return is_string($ticket["TITLE"]) ? $ticket["TITLE"] : '';
    }

    /**
     * @param int $ticketId
     * @return array
     */
    public static function getTicket(int $ticketId){
        if(Loader::includeModule("support")) {
            $by = null;
            $order = ["ID" => "DESC"];
            $lang=LANG;
            $checkRights="N";
            $get_user_name="Y";
            $get_extra_names="Y";
            $is_filtered = null;
            $ticket = CTicket::GetList($by, $order, array("ID" => $ticketId), $is_filtered, $checkRights, $get_user_name, $get_extra_names, $lang, [])->fetch();
            return !is_bool($ticket) ? $ticket : [] ;
        }
        return [];
    }

    /**
     * @param $ticket
     * @return array
     */
    private static function getUserFromTicket($ticket):array{
        $userId = $ticket["OWNER_USER_ID"];
        $user = [];
        if($userId > 0){
           $user = UserTable::getById($userId)->fetch();
        }
        return $user;
    }

    /**
     * @param array $user
     * @return array
     * @throws \Exception
     */
    private static function getUserProject(array $user):array{
        if(Loader::includeModule("socialnetwork")) {
            $userId = $user["ID"];
            $filter = ["USER_ID" => $userId];

            return \is_array(\CSocNetUserToGroup::GetList([], $filter)->Fetch()) ? \CSocNetUserToGroup::GetList([], $filter)->Fetch() : [];

        }else{
            throw new \Exception(\sprintf('Could not load social networks to get user workgroup[02032021.1111.1]'), 500);
        }
    }

    private static function getSupportTask(array $group):int{
        if(Loader::includeModule("tasks")) {
            $groupId = (int)$group["GROUP_ID"];
            if($groupId > 0 && $groupId !== null){
                $task = Tasks::getList(["select" => ["ID"], "filter" => [ "%TITLE" => "support", "GROUP_ID" => $groupId, "STATUS" => 2 ]]);
                $taskId = $task[0]["ID"];

                if (!isset($task[0]["ID"])) {
                    $taskUserFields = UserFieldTable::getList(["filter" => ["ENTITY_ID" => "TASKS_TASK", "FIELD_NAME" => "UF_SUPPORT"]])->fetchAll();

                    $taskValues = [
                        "TITLE" => "Support",
                        "RESPONSIBLE_ID" => Settings::SUPPORT_TASK_RESPONSIBLE,
                        "ACCOMPLICE" => Settings::SUPPORT_TASK_PARTICIPANTS,
                        "GROUP_ID" => $groupId,
                        "STATUS" => 2,
                        "SITE_ID" => "s1",
                        "CREATED_BY" => Settings::SUPPORT_TASK_RESPONSIBLE
                    ];

                    if(!empty($taskUserFields)) $taskValues["UF_SUPPORT"] = 744;

                    $task = Tasks::create($taskValues);

                    if(!$task){
                        throw new \Exception(\sprintf('could not create new support task for this ticket[02032021.1111.1]'), 500);
                        //throw Exception
                    }

                    $taskId = (int)$task->getId();
                }

                return $taskId;
            }else{
                return 0;
            }
        }

        return 0;
    }
}