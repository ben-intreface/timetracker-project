<?php


namespace Intreface\Module\TimeTracker;


use Intreface\Module\TimeTracker\{
    Internals\ActionTimeTable,
    Tasks,
    Tickets,
    Emails,
    EmailRelation,
    Settings
};
use \Bitrix\Main\{
    Type,
    Event,
    Loader,
    UserTable
};

/**
 * Class Action
 * @package Intreface\Module\TimeTracker
 */

class Action
{
    protected $action = null;

    /**
     * Action constructor.
     * @param int $id
     * @throws \Exception
     */
    public function __construct(int $id){
        $this->action = ActionTimeTable::getById($id)->fetchObject();
        if(empty($this->action)){
            throw new \Exception(\sprintf('Action not Found [02032021.1111.1]', $id), 500);
        }
    }

    /**
     * @param array $data
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    private static function create(array $data, int $userId):int{

        $newAction = ActionTimeTable::createObject();
        $newAction->setOriginId((int)isset($data["ORIGIN_ID"]) ? $data["ORIGIN_ID"]: 0);
        $newAction->setOriginType((string)$data["ORIGIN_TYPE"]);
        $newAction->setTaskLogId((int)isset($data["TASK_LOG_ID"]) ? $data["TASK_LOG_ID"]: 0);
        $newAction->setName($data["NAME"]);
        $newAction->setDateCreated(new Type\DateTime());
        $newAction->setUserId($userId);
        $newAction->setLocked((bool)false);
        $result = $newAction->save();

        if(!$result->isSuccess()){
            throw new \Exception(\sprintf('Action Table could not create new Action[02032021.1111.1]', $userId), 500);
        }

        $event = new Event('intreface.module-timetracker', '\\Intreface\\Module\\TimeTracker\\Action::onAfterCreate', [
            "ACTION_ID" => $result->getId()
        ]);
        $event->send();

        return $result->getId();

    }

    /**
     * @param array $data
     * @return array
     */
    public static function getList(array $data):array{
        return ActionTimeTable::getList($data)->fetchAll();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function lock(): bool
    {
        $this->action->setLocked((bool)true);
        $result = $this->action->save();
        if (!$result->isSuccess()) {
            throw new \Exception(\sprintf('could not lock Action[02032021.1111.1]'), 500);
        }
        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function unlock(): bool {
        $this->action->setLocked((bool)false);
        $result = $this->action->save();
        if(!$result->isSuccess()){
            throw new \Exception(\sprintf('could not unlock Action[02032021.1111.1]'), 500);
        }

        $event = new Event('intreface.module-timetracker', '\\Intreface\\Module\\TimeTracker\\Action::onAfterUnlock', [
            "ACTION_ID" => $this->action->getId()
        ]);
        $event->send();

        return true;
    }

    /**
     * @return bool
     */
    public function isMine(): bool{
        Global $USER;
        $actionUser = $this->action->getUserId();
        return (int)$actionUser == (int)$USER->getID();
    }

    /**
     * @param int $userId
     * @param string $originType
     * @param int $originId
     * @return int
     */
    public static function checkDailyActionExists(int $userId, string $originType, int $originId):int{

        $dailyRange = self::getCurrentDateRange();

        $dailyActions = self::getList([
            "select" => [],
            "filter" => [
                "=ORIGIN_ID" => $originId,
                "=ORIGIN_TYPE" => $originType,
                "=USER_ID" => $userId,
                array("LOGIC" => "AND",
                    0 => array(">=DATE_CREATED" => $dailyRange[0]),
                    1 => array("<=DATE_CREATED" => $dailyRange[1]),
                ),
            ],
            "order" => ["ID" => "DESC"]
        ]);

        return isset($dailyActions[0]["UALIAS_0"]) ? $dailyActions[0]["UALIAS_0"] : 0;

    }

    /**
     * @param int $userId
     * @return bool
     */
    private static function checkUserPermissions(int $userId):bool{
        $permission = false;

        if(Loader::includeModule("socialnetwork")){

            $group = UserTable::getUserGroupIds($userId);

            if(empty($group)){
                $permission = false;
            }

            if(\in_array(Settings::PERMISSIONS_USER_GROUP, $group)){
                $permission = true;
            }
        }

        return $permission;

    }


    /**
     * @param int $originId
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function createTaskAction(int $originId, int $userId):int{

        if(self::checkUserPermissions($userId)) {

            $task = new Tasks($originId);

            if (!$task) {
                new \Exception(\sprintf('Cant find Task with this Id [02032021.11111.111]', $userId), 500);
            }

            $actionId = self::checkDailyActionExists($userId, "TASK", $originId);

            if ($actionId === 0) {
                $actionId = Action::create([
                    "NAME" => $task->getName(),
                    "ORIGIN_ID" => $originId,
                    "ORIGIN_TYPE" => "TASK",
                    "TASK_LOG_ID" => $originId],
                    $userId);
            } else {
                $action = new Action($actionId);
                if (!$action->isMine()) {
                    //throw error
                    new \Exception(\sprintf('Action Not yours [02032021.11111.111]', $userId), 500);
                }
                $action->unlock();
            }
            return $actionId;

        }else{
            return 0;
        }


    }

    /**
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function createExternalCallAction(int $userId):int
    {
        if(self::checkUserPermissions($userId)) {
            $loggedId = 0;

            return self::create([
                "NAME" => "External Call",
                "ORIGIN_ID" => 0,
                "ORIGIN_TYPE" => "CALL",
                "TASK_LOG_ID" => $loggedId],
                $userId);
        }else{
            return 0;
        }
    }

    /**
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function createInternalCallAction(int $userId):int
    {
        if(self::checkUserPermissions($userId)) {
            $loggedId = Tasks::getInternalTaskId($userId);

            return self::create([
                "NAME" => "Internal Call",
                "ORIGIN_ID" => 0,
                "ORIGIN_TYPE" => "CALL",
                "TASK_LOG_ID" => $loggedId],
                $userId);
        }else{
            return 0;
        }
    }

    /**
     * @param int $originId
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function createTicketAction(int $originId, int $userId):int{
        if(self::checkUserPermissions($userId)) {
            $actionId = self::checkDailyActionExists($userId, "TICKET", $originId);
            if ($actionId === 0) {
                $actionId = Action::create([
                    "NAME" => strlen(Tickets::getTicketName($originId)) > 0 ? Tickets::getTicketName($originId) : "TICKET NAME NOT FOUND" ,
                    "ORIGIN_ID" => $originId,
                    "ORIGIN_TYPE" => "TICKET",
                    "TASK_LOG_ID" => Tickets::resolveTicketTask($originId)],
                    $userId);
            } else {
                $action = new Action($actionId);
                if (!$action->isMine()) {
                    //throw error
                    new \Exception(\sprintf('Action Not yours [02032021.11111.111]', $userId), 500);
                }
                $action->unlock();
            }

            return $actionId;
        }else{
            return 0;
        }
    }

    /**
     * @param int $originId
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function createEmailAction(int $originId, int $userId):int{
        if(self::checkUserPermissions($userId)) {
            $email = Emails::getEmail($originId);

            if (empty($email)) {
                new \Exception(\sprintf('Cant find Email with this Id [02032021.11111.111]', $userId), 500);
            }

            //guessActionChain
            $actionId = Emails::GuessActionChain($email[0], $email[1], $userId);
            if ($actionId > 0) {
                $action = new Action($actionId);

                if (!$action->isMine()) {
                    //throw error
                    new \Exception(\sprintf('Action Not yours [02032021.11111.111]', $userId), 500);
                }

                $action->unlock();

            } else {

                $actionId = $actionId = Action::create([
                    "NAME" => str_ireplace("Re: ", "", $email[1])." - ".$email[0],
                    "ORIGIN_ID" => $originId,
                    "ORIGIN_TYPE" => "EMAIL",
                    "TASK_LOG_ID" => 0],
                    $userId);
            }

            EmailRelation::create(["SUBJECT" => str_ireplace("Re: ", "", $email[1]), "EMAIL_ADDRESS" => $email[0], "ACTION_ID" => $actionId, "MESSAGE_ID" => $originId], $userId);

            return $actionId;

        }else{
            return 0;
        }

    }

    /**
     * @return Type\DateTime[]
     */
    public static function getCurrentDateRange(): array
    {

        $morning = new Type\DateTime();
        $morning->setTime(0,0, 0, 00);
        $night = new Type\DateTime();
        $night->setTime(23,59,59,999999);

        $range = [$morning, $night];

        return $range;
    }

    /**
     * @param int $seconds
     * @param string $comment
     * @param int $loggedId
     * @return bool
     * @throws \Exception
     */
    public function addTime(int $seconds, string $comment, int $loggedId): bool{

        global $USER;

        if(!$this->action->getTaskLogId() === 0){
            if((int)$this->action->getTaskLogId() !== (int)$loggedId){
                throw new \Exception(\sprintf('Task Log ID does not match [02032021.1111.1]', $USER->GetID()), 500);
            }
        }else{
            $this->action->setTaskLogId($loggedId);
        }

        if($comment !== ''){
            $comment = ' - ' . $comment;
        }

        $timeComment = $this->action->getOriginType() .  $comment;
        $beforeEvent = new Event('intreface.module-timetracker', '\\Intreface\\Module\\TimeTracker\\Action::onBeforeAddTime', [
            'actionId' => $this->action->getId(),
            'userId' => $USER->GetID(),
            'taskLogId' => $this->action->getTaskLogId(),
            'comment' => $timeComment,
            'input' => $seconds
        ]);
        $beforeEvent->send();

        $this->action->setLoggedTime($seconds);
        $this->action->setTotalLogged($this->action->getTotalLogged() + $seconds);

        $result = $this->action->save();

        if(!$result->isSuccess()){
            throw new \Exception(\sprintf('Action could not be saved [02032021.1111.1]', $USER->GetID()), 500);
        }

        $this->lock();

        $afterEvent = new Event('intreface.module-timetracker', '\\Intreface\\Module\\TimeTracker\\Action::onAfterAddTime', [
            'ACTION_ID' => $this->action->getId()
        ]);
        $afterEvent->send();

        return true;

    }

    /**
     * @return int
     */
    public function actionId():int{
        return $this->action->getId();
    }

    /**
     * @return int
     */
    public function loggedId():int{
        return $this->action->getTaskLogId();
    }

    /**
     * @param int $id
     * @return int
     */
    public function setLoggedId(int $id):int{
        return $this->action->setTaskLogId($id);
    }

    /**
     * @return string
     */
    public function name():string{
        return $this->action->getName();
    }

    /**
     * @return mixed
     */
    public function dateCreate(){
        return $this->action->getDateCreated();
    }

    /**
     * @return int
     */
    public function originId():int{
        return $this->action->getOriginId();
    }

    /**
     * @return string
     */
    public function originType():string{
        return $this->action->getOriginType();
    }

    /**
     * @return int
     */
    public function loggedTime():int{
        return $this->action->getLoggedTime();
    }

    /**
     * @return int
     */
    public function userId():int{
        return $this->action->getUserId();
    }

    /**
     * @return int
     */
    public function totalLogged():int{
        return $this->action->getTotalLogged();
    }


}