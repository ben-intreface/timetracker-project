<?php


namespace Intreface\Module\TimeTracker;

use Intreface\Module\TimeTracker\{
    Internals\EmailRelationTable
};

use \Bitrix\Main\{
    Type
};


/**
 * Class EmailRelation
 * @package Intreface\Module\TimeTracker
 */
class EmailRelation
{

    protected $emailRelation = null;

    /**
     * EmailRelation constructor.
     * @param int $id
     * @throws \Exception
     */
    public function __construct(int $id){
        $this->emailRelation = EmailRelationTable::getById($id)->fetchObject();
        if(empty($this->emailRelation)){
            throw new \Exception(\sprintf('Email Relation Table could not find Relation[02032021.1111.1]', $id), 500);
        }
    }

    /**
     * @param array $data
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function create(array $data, int $userId):int
    {
        $emailRelationObject = EmailRelationTable::createObject();
        if(isset($data["MESSAGE_ID"])){
            $emailRelationObject->setMessageId((int)$data["MESSAGE_ID"]);
        }
        if(isset($data["EMAIL_ADDRESS"])){
            $emailRelationObject->setEmailAddress((string)$data["EMAIL_ADDRESS"]);
        }
        if(isset($data["SUBJECT"])) {
            $emailRelationObject->setSubject($data["SUBJECT"]);
        }
        if(isset($data["ACTION_ID"])) {
            $emailRelationObject->setActionId($data["ACTION_ID"]);
        }

        $emailRelationObject->setUserId($userId);
        $emailRelationObject->setDate(new Type\DateTime());
        $result = $emailRelationObject->save();
        if(!$result->isSuccess()){
            throw new \Exception(\sprintf('Email Relation Table could not create new relation[02032021.1111.1]', $userId), 500);
        }
        return $result->getId();

    }

    /**
     * @param array $data
     * @return array
     */
    public static function getList(array $data):array
    {
        return EmailRelationTable::getList((array) $data)->fetchAll();
    }

}