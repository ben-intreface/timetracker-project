<?php


namespace Intreface\Module\TimeTracker;

use Bitrix\Main\{
    Loader
};
use Intreface\Module\TimeTracker\{
    Action,
    EmailRelation,
    Settings
};
use Bitrix\Mail\MailMessageTable;

/**
 * Class Emails
 * @package Intreface\Module\TimeTracker
 */
class Emails extends Action
{

    /**
     * @param int $emailId
     * @return array
     * @throws \Exception
     */
    public static function getEmail(int $emailId): array{
        if(Loader::includeModule("mail")) {
            global $USER;
            $email = MailMessageTable::getById($emailId)->fetch();

            if(!$email){
                new \Exception(\sprintf('Cant find Email with this Id [02032021.11111.111]', $USER->getID()), 500);
            }
            $subject = $email["SUBJECT"];
            $emails = [$email["FIELD_FROM"], $email["FIELD_TO"]];
            $emailAdd = self::resolveEmailAddress($emails);

            if(strlen(trim($emailAdd)) > 0){
                $data = [$emailAdd, $subject];
            }else{
                $data = ["Could not Resolve Email", $subject];
            }

            return $data;
        }else{
            throw new \Exception(\sprintf('Could not load mail to get email [02032021.1111.1]'), 500);
        }
    }


    /**
     * @param array $emails
     * @return string
     */
    private static function resolveEmailAddress(array $emails): string{

        $email = '';
        if($emails !== null) {
            $internalAdd = Settings::INTERNAL_EMAIL_DOMAIN;

            if (!preg_match("/{$internalAdd}/", $emails[0]) && preg_match("/{$internalAdd}/", $emails[1])) {
                $email = $emails[0];
            } elseif (!preg_match("/{$internalAdd}/", $emails[1]) && preg_match("/{$internalAdd}/", $emails[0])) {
                $email = $emails[1];
            } elseif ($emails[0] === null || $emails[1] === null) {
                $email = '';
            } else {
                $email = Settings::INTERNAL_EMAIL_PLACEHOLDER;
                echo "last";
            }
        }

        return $email;

    }

    /**
     * @param string $email
     * @param string $subject
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function guessActionChain(string $email, string $subject, int $userId): int{
        $dateRange = Action::getCurrentDateRange();
        $actionId = 0;

        if($email !==  Settings::INTERNAL_EMAIL_PLACEHOLDER && $email !== '') {
            //logic to use email address and email subject to get list of Email Action relations

            $emailAction = EmailRelation::getList([
                "select" => ["ACTION_ID"],
                "filter" => [
                    "=SUBJECT" => str_ireplace("Re: ", "", $subject),
                    "%EMAIL_ADDRESS" => $email,
                    "USER_ID" => $userId,
                    array("LOGIC" => "AND",
                        0 => array(">=DATE" => $dateRange[0]),
                        1 => array("<=DATE" => $dateRange[1]),
                    ),
                ],
                "order" => ["ID" => "DESC"]
            ]);


        }else{
            $emailAction = EmailRelation::getList([
                "select" => ["ACTION_ID"],
                "filter" => [
                    "=SUBJECT" => str_ireplace("Re: ", "", $subject),
                    "USER_ID" => $userId,
                    array("LOGIC" => "AND",
                        0 => array(">=DATE" => $dateRange[0]),
                        1 => array("<=DATE" => $dateRange[1]),
                    ),
                ],
                "order" => ["ID" => "DESC"]
            ]);
        }

        if(isset($emailAction[0]["ACTION_ID"])){
            $actionObject = new Action($emailAction[0]["ACTION_ID"]);
            if($actionObject){
                $actionId = $actionObject->actionId();
            }
        }

        return $actionId;

    }

}