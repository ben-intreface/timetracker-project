<?php


namespace Intreface\Module\TimeTracker;

use Intreface\Module\TimeTracker\Internals\{
    ActionLogTable
};
use \Bitrix\Main\{
    Type
};

/**
 * Class ActionLogItem
 * @package Intreface\Module\TimeTracker
 */

class ActionLogItem
{

    protected $actionLogItem = null;

    /**
     * ActionLogItem constructor.
     * @param int $id
     * @throws \Exception
     */
    public function __construct(int $id){
        $this->actionLogItem = ActionLogTable::getById($id)->fetchObject();
        if(empty($this->actionLogItem)){
            throw new \Exception(\sprintf('Action Log Item Could not be created [02032021.1111.1]'), 500);
        }
    }

    /**
     * @param array $data
     * @return int
     * @throws \Exception
     */
    public static function create(array $data): int{
        $actionLogObject = ActionLogTable::createObject();

        $actionLogObject->setActionName($data["NAME"]);
        $actionLogObject->setActionId($data["ACTION_ID"]);
        if(isset($data["ORIGIN_ID"])) {
             $actionLogObject->setOriginId((int)$data["ORIGIN_ID"]);
        }
        if(isset($data["ORIGIN_TYPE"])) {
            $actionLogObject->setOriginType((string)$data["ORIGIN_TYPE"]);
        }
        if(isset($data["TASK_LOG_ID"])) {
            $actionLogObject->setTaskLogId((int)$data["TASK_LOG_ID"]);
        }
        if(isset($data["LOGGED_TIME"])){
            $actionLogObject->setLoggedTime((int)$data["LOGGED_TIME"]);
        }
        if(isset($data["TOTAL_LOGGED"])){
            $actionLogObject->setTotalLogged((int)$data["TOTAL_LOGGED"]);
        }
        $actionLogObject->setDateCreated(new Type\DateTime());
        $actionLogObject->setActionDateCreated($data["ACTION_DATE_CREATED"]);
        $actionLogObject->setUserId($data["USER_ID"]);

        $result = $actionLogObject->save();

        if(!$result->isSuccess()){
            throw new \Exception(\sprintf('Action Log Table could not create new Action[02032021.1111.1]', $data["USER_ID"]), 500);
        }

        return $result->getId();

    }

}