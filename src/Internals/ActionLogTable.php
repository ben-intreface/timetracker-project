<?php


namespace Intreface\Module\TimeTracker\Internals;

use \Bitrix\Main\ORM\{
    Data\DataManager,
    Fields,
    Query\Join
};
use \Bitrix\Tasks\Internals\{ TaskTable };
use \Bitrix\Main\{ UserTable };
use \Intreface\Module\TimeTracker\Internals\{
    ActionTimeTable
};


class ActionLogTable extends DataManager
{

    public static function getTableName(): string {
        return "i_module_timetracker_actions_log";
    }

    public static function getMap(): array {
        return [
            new Fields\IntegerField("ID", [
                "autocomplete" => true,
                "primary" => true,
            ]),
            new Fields\IntegerField("ACTION_ID", [
                "required" => true
            ]),
            new Fields\StringField("ACTION_NAME", [
                "required" => true,
            ]),
            new Fields\IntegerField("ORIGIN_ID", [
                "required" => true,
                "default_value" => 0
            ]),
            new Fields\StringField("ORIGIN_TYPE", [
                "required" => true,
            ]),
            new Fields\IntegerField("USER_ID", [
                "require" => true
            ]),
            new Fields\IntegerField("TASK_LOG_ID", [
                "required" => false,
            ]),
            new Fields\DatetimeField("ACTION_DATE_CREATED", [
                "required" => true,
            ]),
            new Fields\DatetimeField("DATE_CREATED", [
                "required" => true,
            ]),
            new Fields\IntegerField("LOGGED_TIME", [
                "required" => false
            ]),
            new Fields\IntegerField("TOTAL_LOGGED", [
                "required" => false
            ]),
            (new Fields\Relations\Reference(
                "ACTION",
                ActionTimeTable::class,
                Join::on('this.ACTION_ID', 'ref.ID'))
            )->configureJoinType("inner"),
            (new Fields\Relations\Reference(
                "TASK",
                TaskTable::class,
                Join::on('this.TASK_LOG_ID', 'ref.ID'))
            )->configureJoinType("inner"),
            (new Fields\Relations\Reference(
                "USER",
                UserTable::class,
                Join::on('this.USER_ID', 'ref.ID'))
            )->configureJoinType("inner"),
        ];
    }

    public static function getUfId(): string {
        return \mb_strtoupper(\mb_substr(static::getTableName(), 2));
    }

}