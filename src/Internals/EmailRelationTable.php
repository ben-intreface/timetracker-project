<?php

declare(strict_types=1);

namespace Intreface\Module\TimeTracker\Internals;

use \Bitrix\Main\ORM\{
    Data\DataManager,
    Fields,
    Query\Join
};
use Bitrix\Sender\MailingTable;
use Intreface\Module\TimeTracker\Internals\ActionTimeTable;
use Bitrix\Main\UserTable;

class EmailRelationTable extends DataManager
{

    public static function getTableName(): string {
        return "i_module_timetracker_emails";
    }

    public static function getMap(): array {
        return [
            new Fields\IntegerField("ID", [
                "autocomplete" => true,
                "primary" => true,
            ]),
            new Fields\IntegerField("MESSAGE_ID", [
                "required" => true,
            ]),
            new Fields\StringField("EMAIL_ADDRESS", [
                "required" => true,
            ]),
            new Fields\StringField("SUBJECT", [
                "require" => true
            ]),
            new Fields\DatetimeField("DATE", [
                "required" => false,
            ]),
            new Fields\IntegerField("USER_ID", [
                "required" => true,
            ]),
            new Fields\IntegerField("ACTION_ID"),
            (new Fields\Relations\Reference(
                "EMAIL",
                MailingTable::class,
                Join::on('this.MESSAGE_ID', 'ref.ID'))
            )->configureJoinType("inner"),
            (new Fields\Relations\Reference(
                "ACTION",
                ActionTimeTable::class,
                Join::on('this.ACTION_ID', 'ref.ID'))
            )->configureJoinType("inner"),
            (new Fields\Relations\Reference(
                "USER",
                UserTable::class,
                Join::on('this.USER_ID', 'ref.ID'))
            )->configureJoinType("inner"),
        ];
    }


}