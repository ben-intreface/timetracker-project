<?php


namespace Intreface\Module\TimeTracker;

use Bitrix\Tasks\Internals\TaskTable;
use Bitrix\Main\{
    Loader,
    UserFieldTable
};

use Intreface\Module\TimeTracker\{
    Settings
};

/**
 * Class Tasks
 * @package Intreface\Module\TimeTracker
 */
class Tasks
{

    protected $task = null;

    /**
     * Tasks constructor.
     * @param int $id
     * @throws \Exception
     */
    public function __construct(int $id){
        if(Loader::includeModule("tasks")) {
            $this->task = TaskTable::getById($id)->fetchObject();
            if (empty($this->task)) {
                throw new \Exception(\sprintf('Task not Found [02032021.1111.1]', $id), 500);
            }
        }
    }

    /**
     * @param array $fields
     * @param int $loggedId
     * @param int $userId
     * @return bool
     */
    public function addElapsedTime(array $fields, int $loggedId, int $userId): bool
    {
        if(Loader::includeModule("tasks")) {
            // logic to add current time to Task

            $taskInterface = new \CTaskItem($loggedId, $userId);

            \CTaskElapsedItem::add($taskInterface, $fields);

            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $name = '';
        if(Loader::includeModule("tasks")) {
            // logic to add current time to Task
            $name = $this->task->getTitle();
        }
        return $name;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public static function getList(array $params):array{
        if(Loader::includeModule("tasks")) {
            $params["filter"]["ZOMBIE"] = "N";
            return TaskTable::getList($params)->fetchAll();
        }
        return [];
    }

    /**
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public static function create(array $data):object {
        if(Loader::includeModule("tasks")) {
            $task = \CTaskItem::add($data, 1, []);

            if(!$task) {
                // throw Exception
                throw new \Exception(\sprintf('could not add new Task[02032021.1111.1]'), 500);
            }

            return $task;
        }else{
            // throw Exception
            throw new \Exception(\sprintf('Could not load tasks to to create new Task [02032021.1111.1]'), 500);
        }
    }

    /**
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public static function getInternalTaskId(int $userId):int
    {
        $tasks = Tasks::getList([
            "order" => ["ID" => "DESC"],
            "filter" => [
                "%TITLE" => "Tasks out of Task Management",
                "RESPONSIBLE_ID" => $userId,
                "GROUP_ID" => Settings::INTERNAL_GROUP_ID,
                "STATUS" => 2,
            ],
            "select" => ["ID", "TITLE"],
        ]);

        if ($tasks) {

            return $tasks[0]["ID"]; // Need to extract ID from task object

        } else {

            $internalTasks = Tasks::getList([
                "order" => ["ID" => "DESC"],
                "filter" => [
                    "%TITLE" => "internal",
                    "RESPONSIBLE_ID" => $userId,
                    "GROUP_ID" => Settings::INTERNAL_GROUP_ID,
                    "STATUS" => 2,
                ],
                "select" => ["ID", "TITLE"],
            ]);

            if ($internalTasks) {

                return $internalTasks[0]["ID"]; // Need to extract ID from task object

            }else {

                $taskUserFields = UserFieldTable::getList(["filter" => ["ENTITY_ID" => "TASKS_TASK", "FIELD_NAME" => "UF_SUPPORT"]])->fetchAll();

                $taskValues = [
                    "TITLE" => "Internal Task",
                    "RESPONSIBLE_ID" => $userId,
                    "GROUP_ID" => Settings::INTERNAL_GROUP_ID,
                    "SITE_ID" => 's1',
                    "CREATED_BY" => $userId
                ];

                if (!empty($taskUserFields)) $taskValues["UF_SUPPORT"] = 743;

                $task = Tasks::create($taskValues);

                if (!$task) {
                    //throw new exception
                    new \Exception(\sprintf('Could not find task - Internal task creation failed [02032021.11111.111]', $userId), 500);
                }

                return $task->getId();
            }

        }
    }

}