<?php


/**
 * Author Ben Berhanu
 * With contribution from Mikkie
 */

namespace Intreface\Module\TimeTracker\Controller;

use Intreface\Module\TimeTracker\{
    Action,
    Tasks
};

use Bitrix\Main\{
    Engine\Controller,
    Loader,
    Engine\ActionFilter
};

use Bitrix\Socialnetwork\{
    WorkgroupTable
};

/**
 * Class Tracker
 * @package Intreface\Module\TimeTracker\Controller
 */
class Tracker extends Controller
{

    /**
     * @return \array[][]
     * register controller for execusion via AJAX
     */
    public function configureActions(): array{
        $preFilters = [
            new ActionFilter\Authentication(),
            new ActionFilter\HttpMethod(
                [ActionFilter\HttpMethod::METHOD_POST, ActionFilter\HttpMethod::METHOD_GET]
            ),
            new ActionFilter\Csrf()
        ];

        return [
            'debug' => [
                'prefilters' => $preFilters
            ],

            'addInternalCall' => [
                'prefilters' => $preFilters
            ],

            'addExternalCall' => [
                'prefilters' => $preFilters
            ],

            'getTaskList' => [
                'prefilters' => $preFilters
            ],

            'getUserDailyActionList' => [
                'prefilters' => $preFilters
            ],

            'getFullUserDailyActionList' => [
                'prefilters' => $preFilters
            ],

            'getUserFullActionList' => [
                'prefilters' => $preFilters
            ],

            'dismiss' => [
                'prefilters' => $preFilters
            ],

            'save' => [
                'prefilters' => $preFilters
            ],
        ];
    }

    /**
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public static function addInternalCallAction(int $userId):bool
    {

        $actionId = Action::createInternalCallAction($userId);

        return $actionId > 0;

    }

    /**
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public static function addExternalCallAction(int $userId):bool
    {

        $actionId = Action::createExternalCallAction($userId);

        return $actionId > 0;

    }

    /**
     * @param int $originId
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public static function addTaskAction(int $originId, int $userId):bool
    {

        $actionId = Action::createTaskAction($originId, $userId);

        return $actionId > 0;

    }

    /**
     * @param int $originId
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public static function addTicketAction(int $originId, int $userId):bool
    {

        $actionId = Action::createTicketAction($originId, $userId);

        return $actionId > 0;

    }

    /**
     * @param int $originId
     * @param int $userId
     * @return bool
     * @throws \Exception
     */
    public static function addEmailAction(int $originId, int $userId):bool
    {

        $actionId = Action::createEmailAction($originId, $userId);

        return $actionId > 0;

    }

    /**
     * @return array
     * Gets list of tasks and returns array for front end drop down selection
     */
    public static function getTaskListAction():array{
        $tasksList = Tasks::getList(["select" => ["ID", "TITLE", "GROUP_ID"]]);
        $tasks = [];
        foreach($tasksList as $task){
            $taskArray = ["value" => $task["ID"]];
            if(isset($task["GROUP_ID"]) && $task["GROUP_ID"] > 0){
                if(Loader::includeModule("socialnetwork")) {
                    $group = WorkgroupTable::getById($task["GROUP_ID"])->fetch();
                    $taskArray["label"] = $group["NAME"]. " - " . $task["TITLE"];
                }
            }else{
                $taskArray["label"] = $task["TITLE"];
            }
            $tasks[] = $taskArray;
        }
        return $tasks;
    }

    /**
     * @param int $actionId
     * @param int $taskLogId
     * @param int $seconds
     * @param string $comment
     * @param int $userId
     * @return bool
     * @throws \Exception
     * Saves action
     */
    public static function saveAction(int $actionId, int $taskLogId, int $seconds, string $comment, int $userId){
        $action = new Action($actionId);

        if(!$action->isMine()) {
            return false;
        }

        $result = $action->addTime($seconds, $comment, $taskLogId);
        if($result){
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public static function dismissAction(int $id):bool{
        $action = new Action($id);
        if(!$action->isMine()) {
            return false;
        }
        $result = $action->lock();
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getUserDailyActionListAction(int $userId):array{

        $dailyRange = Action::getCurrentDateRange();

        return Action::getList(["filter" => [
            "=USER_ID" => $userId,
            "=LOCKED" => "N",
            array("LOGIC" => "AND",
                0 => array(">=DATE_CREATED" => $dailyRange[0]),
                1 => array("<=DATE_CREATED" => $dailyRange[1]),
            ),
        ]]);

    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getFullUserDailyActionListAction(int $userId):array{

        $dailyRange = Action::getCurrentDateRange();

        return Action::getList(["filter" => [
            "=USER_ID" => $userId,
            array("LOGIC" => "AND",
                0 => array(">=DATE_CREATED" => $dailyRange[0]),
                1 => array("<=DATE_CREATED" => $dailyRange[1]),
            ),
        ]]);

    }

    /**
     * @param int $userId
     * @return array
     */
    public static function getUserFullActionListAction(int $userId):array{
        return Action::getList(["filter" => [
            "=USER_ID" => $userId
        ]]);
    }

}