<?php

declare(strict_types=1);

use \Bitrix\Main\{
    Application
};
use Intreface\Module\TimeTracker\Internals;

$connection = Application::getConnection();

if (!$connection->isTableExists(Internals\ActionTimeTable::getTableName())) {
    Internals\ActionTimeTable::getEntity()->createDbTable();
}
if (!$connection->isTableExists(Internals\EmailRelationTable::getTableName())) {
    Internals\EmailRelationTable::getEntity()->createDbTable();
}

if (!$connection->isTableExists(Internals\ActionLogTable::getTableName())) {
    Internals\ActionLogTable::getEntity()->createDbTable();
}