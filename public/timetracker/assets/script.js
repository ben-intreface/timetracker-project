import { createApp, ref, computed, onBeforeMount, reactive } from '/local/assets/package/intreface.helper-ui/js/vue/vue.js';
import { useStore } from '/local/assets/package/intreface.helper-ui/js/store/store.js';
import Quasar, { Notify } from '/local/assets/package/intreface.helper-ui/js/quasar/quasar.js';


const template = ` 
 
 <div class="q-pa-md q-app box-border-inherit">
        <q-table :rows="getRecords" :columns="columns" :loading="loading" title="My Time Tracker" :loading="loading" row-key="ID" virtual-scroll :pagination.sync="pagination" :rows-per-page-options="[0]" dense style="max-height: 600px">
            <template slot="loading">Loading...</template>
            <template v-slot:body="props">
                <q-tr :props="props">
                    <q-td key="ID" :props="props">
                        {{ props.row.ID }}
                    </q-td>
                    <q-td key="name" :props="props" style="white-space: normal; width:300px;">
                        {{ props.row.name }}
                    </q-td>
                    <q-td key="type" :props="props">
                        {{ props.row.type }}
                    </q-td>
                    <q-td key="date" :props="props">
                        {{ props.row.date }}
                    </q-td>
                    <q-td key="task" :props="props" style="width: 180px; white-space: normal;">
                        <template v-if="!props.row.task">
                            <q-select bottom-slots v-model="records.data[props.row.ID].task" label="Task" :dense="true"
                                options-dense="denseOpts" use-input debounce="500" emit-value :options="getOptions" @filter="action.tasks.filterFn" @click="action.tasks.getTaskData()">
                                <template v-slot:append>
                                    <q-icon v-if="model !== ''" name="close" @click.stop="model = ''"
                                        class="cursor-pointer" />
                                    <q-icon name="search" @click.stop />
                                </template>
                            </q-select>
                        </template>
                        <template v-if="props.row.task > 0 && options.data">
                            <template  v-if="options.data[props.row.task]">
                            <a v-bind:href=taskLink+props.row.task+"/" target="_blank">{{ options.data[props.row.task].label }}</a>
                            </template>
                            <template v-else>Task Deleted</template>
                        </template>                        
                    </q-td>
                    <q-td key="total" :props="props">
                        <div class="text-pre-wrap">{{ props.row.totalHours }} Hours & {{ props.row.totalMinutes }} Minutes</div>
                    </q-td>
                    <q-td key="last" :props="props">
                        <div class="text-pre-wrap">{{ props.row.lastHours }} Hours & {{ props.row.lastMinutes }} Minutes</div>
                    </q-td>
                    <q-td key="comment" :props="props" style="max-width: 300px; white-space: normal;">
                        <div class="text-pre-wrap"><q-input type="text" v-model="records.data[props.row.ID].comment" dense autofocus emit-value /></div>
                    </q-td>
                    <q-td key="time" :props="props" style="width: 150px;">
                              <div class="q-pa-md" >
                                <div style="width: 40%; float:left; margin-right: 5px;">
                                    <q-input type="number" v-model.number="records.data[props.row.ID].timeHours"  dense autofocus min="0" hint="Hours" emit-value />
                                </div>
                                <div style="width: 40%; float: right; margin-right: 5px;">
                                    <q-input type="number" v-model.number="records.data[props.row.ID].timeMinutes" dense autofocus min="0" max="59" hint="Minutes" emit-value />
                                </div>
                            </div>
                    </q-td>
                    <q-td key="option" :props="props" style="max-width: 30px">
                        <template v-if="props.row.task > 0 && options.data">
                            <template  v-if="options.data[props.row.task]">
                                <q-btn unelevated rounded size="9px" text-color="white" :loading="props.row.saveLoading" label="save" :key="action" style="margin-right: 5px; background:#3bc8f5;" @click.stop="action.actions.save(props.row)"/>
                            </template>
                        </template>
                    </q-td>
                </q-tr>
            </template>
            <template v-slot:top-right>
                <q-btn-group unelevated>
                    <q-btn unelevated padding="10px" text-color="white" icon="add" label="Internal Call" :key="action" @click.stop="action.actions.addIntCall()" style="border-radius: 0px; background:#3bc8f5;" :loading="internalCallLoading" split/>   
                    <q-separator dark vertical />
                    <q-btn unelevated padding="10px" text-color="white" icon="add" label="External Call" :key="action" @click.stop="action.actions.addExtCall()" style="border-radius: 0px;  background:#3bc8f5;" :loading="externalCallLoading" split/> 
                </q-btn-group>
            </template>
        </q-table>
    </div>`;

const app = createApp({
    name: "Timetracker",
    template,
    setup(){
        const pagination = {
            rowsPerPage: 0,
            sortBy: 'ID',
            descending: true
        }

        const taskURL = "https://portal.intreface.com/company/personal/user/"

        const taskLink = computed(() => {
            let user = BX.message("USER_ID")
            return taskURL + user + "/tasks/task/view/"

        })

        const columns = [
            { name: 'ID', align: 'left', label: 'ID', field: 'ID', sortable: true,  sort: (a, b) => a - b  },
            { name: 'name', align: 'left', label: 'Name', field: 'name' },
            { name: 'type', align: 'center', label: 'Type', field: 'type' },
            { name: 'date', align: 'center', label: 'Date', field: 'date' },
            { name: 'task', align: 'center', label: 'Task', field: 'task' },
            { name: 'total', label: 'Total Logged', field: 'totalHours & totalMinutes', align: 'center' },
            { name: 'last', label: 'Last Logged', field: 'lastHours & lastMinutes', align: 'center' },
            { name: 'comment', label: 'Comment', field: 'comment', align: 'center' },
            { name: 'time', label: 'Log Time', field: 'timeHours & timeMinutes', align: 'center' },
            { name: 'option', label: 'Options', align: 'center'}
        ]

        const records = reactive({data: []})

        const getRecords = computed(() => {
            let actions = [];
            Object.values(records.data).forEach(y => {
                actions.push({...y})
            })
            return actions
        })

        const options = reactive({data: [], dataFiltered: []})

        const getOptions = computed(() => {
            let tasks = [];
            Object.values(options.dataFiltered).forEach(x => {
                tasks.push({...x})
            })
            return tasks
        })

        let loading = ref(false)

        let internalCallLoading = ref(false)

        let externalCallLoading = ref(false)

        const action = {
            actions: {
                save(row) {
                    loading.value = true
                    row.saveLoading = true
                    let seconds = action.actions.convertToSeconds(row.timeHours, row.timeMinutes)
                    if(seconds > 240) {
                        BX.ajax.runAction('intreface:module.timetracker.Tracker.save', { data: {
                                actionId:row.ID,
                                taskLogId:row.task,
                                seconds: seconds,
                                comment: row.comment,
                                userId: BX.message("USER_ID") }
                        }).then(response => {
                            Notify.create({
                                group: false,
                                message: 'Saved',
                                color: 'green'
                            })
                            loading.value = false
                            row.saveLoading = false
                            row.timeHours = 0
                            row.timeMinutes = 0
                            action.actions.updateUserActions()
                        }).catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not save action ID:'+row.ID + ' - ' + e,
                                color: 'red'
                            })
                            row.saveLoading = false
                            loading.value = false
                        })
                    }else{
                        Notify.create({
                            group: false,
                            message: 'Error',
                            caption: 'Could not save value for action ID:'+row.ID + ' entered values under 5 minutes',
                            color: 'red'
                        })
                        row.saveLoading = false
                        loading.value = false
                    }
                },

                updateUserActions(){
                    loading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getUserFullActionList', { data: { userId: BX.message("USER_ID") } })
                        .then(response => {
                            response.data.forEach(record => {
                                let totalFormattedTime = action.actions.convertFromSeconds(record['TOTAL_LOGGED'])
                                let lastFormattedTime = action.actions.convertFromSeconds(record['LOGGED_TIME'])
                                if(records.data[record.ID]){
                                    records.data[record.ID]["ID"] = record["ID"]
                                    records.data[record.ID]["name"] = record["NAME"]
                                    records.data[record.ID]["task"] = record["TASK_LOG_ID"] > 0 ? record["TASK_LOG_ID"] : null
                                    records.data[record.ID]["type"] = record["ORIGIN_TYPE"]
                                    records.data[record.ID]["date"] = record["DATE_CREATED"].slice(0,10)
                                    records.data[record.ID]["totalHours"] = totalFormattedTime[0]
                                    records.data[record.ID]["totalMinutes"] = totalFormattedTime[1]
                                    records.data[record.ID]["lastHours"] = lastFormattedTime[0]
                                    records.data[record.ID]["lastMinutes"] = lastFormattedTime[1]
                                    records.data[record.ID]["comment"] = ''
                                    records.data[record.ID]["timeHours"] = records.data[record.ID]["timeHours"] > 0 ? records.data[record.ID]["timeHours"] : 0
                                    records.data[record.ID]["timeMinutes"] = records.data[record.ID]["timeMinutes"] > 0 ? records.data[record.ID]["timeMinutes"] : 0
                                    records.data[record.ID]["saveLoading"] = false
                                }else{
                                    records.data[record.ID] = ({
                                        ID: record["ID"],
                                        name: record["NAME"],
                                        task: record["TASK_LOG_ID"] > 0 ? record["TASK_LOG_ID"] : null,
                                        type: record["ORIGIN_TYPE"],
                                        date: record["DATE_CREATED"].slice(0,10),
                                        totalHours: totalFormattedTime[0],
                                        totalMinutes: totalFormattedTime[1],
                                        lastHours: lastFormattedTime[0],
                                        lastMinutes: lastFormattedTime[1],
                                        comment: '',
                                        timeHours: 0,
                                        timeMinutes: 0,
                                        saveLoading: false,
                                    })
                                }
                            })
                            loading.value = false
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not get Actions - ' + e[1],
                                color: 'red'
                            })
                        })
                },

                getUserActions() {
                    loading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getUserFullActionList', { data: { userId: BX.message("USER_ID") } })
                        .then(response => {
                            response.data.forEach(record => {
                                let totalFormattedTime = action.actions.convertFromSeconds(record['TOTAL_LOGGED'])
                                let loggedFormattedTime = action.actions.convertFromSeconds(record['LOGGED_TIME'])
                                records.data[record.ID] =  {
                                    ID: record.ID,
                                    name: record["NAME"],
                                    task: record["TASK_LOG_ID"] > 0 ? record["TASK_LOG_ID"] : null,
                                    type: record["ORIGIN_TYPE"],
                                    date: record["DATE_CREATED"].slice(0,10),
                                    totalHours: totalFormattedTime[0],
                                    totalMinutes: totalFormattedTime[1],
                                    lastHours: loggedFormattedTime[0],
                                    lastMinutes: loggedFormattedTime[1],
                                    comment: '',
                                    timeHours: 0,
                                    timeMinutes: 0,
                                    saveLoading: false
                                }
                            })
                            loading.value = false
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not get Actions - ' + e[1],
                                color: 'red'
                            })
                        })
                },

                convertFromSeconds(seconds){
                    let hours = Math.floor(seconds / 60 / 60);
                    let minutes = Math.floor((seconds / 60) - (hours * 60))
                    return [hours, minutes]
                },

                convertToSeconds(hours, minutes){
                    let seconds = (hours * 60 * 60) + (minutes * 60);
                    return seconds
                },

                addIntCall() {
                    loading.value = true
                    externalCallLoading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.addInternalCall', {
                        data: { originId: 0, userId: BX.message("USER_ID") }
                    }).then(response => {
                        loading.value = false
                        externalCallLoading.value = false
                        Notify.create({
                            group: false,
                            message: 'Success',
                            caption: 'New Internal Call Added' ,
                            color: 'green'
                        })
                        action.actions.updateUserActions()
                    })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could add new call - ' + e[1],
                                color: 'red'
                            })
                            externalCallLoading.value = false
                            loading.value = false
                        })
                },

                addExtCall() {
                    loading.value = true
                    internalCallLoading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.addExternalCall', {
                        data: { originId: 0, userId: BX.message("USER_ID") }
                    }).then(response => {
                        loading.value = false
                        internalCallLoading.value = false
                        Notify.create({
                            group: false,
                            message: 'Success',
                            caption: 'New External Call Added' ,
                            color: 'green'
                        })
                        action.actions.updateUserActions()
                    })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could add new call - ' + e[1],
                                color: 'red'
                            })
                            internalCallLoading.value = false
                            loading.value = false
                        })
                }
            },

            tasks: {
                getTaskData() {
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getTaskList')
                        .then(response => {
                            response.data.forEach(record => {
                                options.data[record.value] = record
                            });
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not load tasks',
                                color: 'red'
                            })
                        })
                },

                filterFn (val, update, abort) {
                    setTimeout(() => {
                        update(
                            () => {
                                if (val === '') {
                                    options.dataFiltered = options.data
                                } else {
                                    const needle = val.toLowerCase()
                                    options.dataFiltered = options.data.filter(v => v.label.toLowerCase().indexOf(needle) > -1)
                                }
                            }
                        )
                    }, 300)
                }
            }
        }

        onBeforeMount(() => {
            action.tasks.getTaskData();
            action.actions.getUserActions();
        });

        return{
            records,
            columns,
            options,
            getRecords,
            pagination,
            action,
            loading,
            taskURL,
            taskLink,
            getOptions
        }
    }
});

app.use(Quasar, {
    plugins: { Notify }
});
app.mount("#timeTrackerFull");




