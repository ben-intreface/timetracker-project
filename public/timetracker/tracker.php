<?php
use \Intreface\Module\TimeTracker\{
    Settings
};

global $APPLICATION;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Time Tracker");

global $USER;
if(\in_array(Settings::PERMISSIONS_USER_GROUP, $USER->GetUserGroupArray())) {
?>


<link rel="stylesheet" type="text/css" href="/local/assets/package/intreface.helper-ui/js/quasar/quasar.css">
<link rel="stylesheet" type="text/css" href="/timetracker/assets/style.css">
<script type ="module" src="/timetracker/assets/script.js?<?time()?>"></script>
<div id="timeTrackerFull"></div>



<?
}else{
    print_r("You do not have permissions to view this page");
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
