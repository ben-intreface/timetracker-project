import { createApp, computed, onBeforeMount, reactive, ref } from '/local/assets/package/intreface.helper-ui/js/vue/vue.js';
import { useStore } from '/local/assets/package/intreface.helper-ui/js/store/store.js';
import Quasar, { Notify } from '/local/assets/package/intreface.helper-ui/js/quasar/quasar.js';

const template = ` 
<div class="q-pa-md q-app box-border-inherit">

<q-bar style="background: none !important; margin-bottom:10px;">

   <div class="gt-md"><h3 style="font-weight: normal;">My Time Tracker</h3></div>
    <q-space />
    
<template v-if="!view">
   <q-chip dense>
        <q-avatar text-color="white" style="background:#3bc8f5;">{{ dailyActions }}</q-avatar>
         <span style="font-size: small">
            <template v-if="dailyActions === 1">
                Action
            </template>
            <template v-else>Actions</template>
        </span>
        </q-chip>
    <q-list separator>
     <q-item>
          <q-item-section>
              <q-item-label overline>Total Logged</q-item-label>
              <q-item-label style="font-size: small">{{ totalLogged }}</q-item-label>
          </q-item-section>
      </q-item>
    </q-list>
</template>
      <q-space />
     <q-btn-group unelevated>
        <q-btn unelevated padding="10px" text-color="white" icon="add" label="Internal Call" :key="action" @click.stop="action.actions.addIntCall()" style="border-radius: 0px; background:#3bc8f5;" :loading="internalCallLoading" split/>   
        <q-separator dark vertical />
        <q-btn unelevated padding="10px" text-color="white" icon="add" label="External Call" :key="action" @click.stop="action.actions.addExtCall()" style="border-radius: 0px;  background:#3bc8f5;" :loading="externalCallLoading" split/>   
        <q-separator dark vertical />          
        <q-btn unelevated padding="10px" text-color="white" label="Toggle View" :key="action" style="border-radius: 0px; background:#3bc8f5;" @click.stop="action.table.toggle()" split/> 
    </q-btn-group> 
</q-bar>

    <template v-if="view">
        <q-table :rows="getRecords" :columns="columns" :loading="loading" :loading="loading" row-key="ID" virtual-scroll :pagination.sync="pagination" :rows-per-page-options="[0]" dense style="height: 400px;">
           <template slot="loading">Loading...</template>
            <template v-slot:body="props">
                <q-tr :props="props">
                    <q-td key="id" :props="props">
                        {{ props.row.id }}
                    </q-td>
                    <q-td key="name" :props="props" style="white-space: normal; width: 280px;">
                        {{ props.row.name }}
                    </q-td>
                    <q-td key="type" :props="props">
                        {{ props.row.type }}
                    </q-td>
                    <q-td key="task" :props="props" style="width: 180px; white-space: normal;">
                        <template v-if="!props.row.task">
                            <q-select bottom-slots v-model="records.data[props.row.id].task" label="Task" :dense="true"
                                options-dense="denseOpts" use-input debounce="500" emit-value :options="getOptions" @filter="action.tasks.filterFn">
                                <template v-slot:append>
                                    <q-icon v-if="model !== ''" name="close" @click.stop="model = ''"
                                        class="cursor-pointer" />
                                    <q-icon name="search" @click.stop />
                                </template>
                            </q-select>
                        </template>
                        <template v-if="props.row.task > 0 && options.data">
                            <template  v-if="options.data[props.row.task]">
                            <a v-bind:href=taskLink+props.row.task+"/" target="_blank">{{ options.data[props.row.task].label }}</a>
                            </template>
                            <template v-else>Task Deleted</template>
                        </template>
                    </q-td>
                    <q-td key="total" :props="props">
                        <div class="text-pre-wrap">{{ props.row.totalHours }} Hours & {{ props.row.totalMinutes }} Minutes </div>
                    </q-td>
                    <q-td key="comment" :props="props" style="width: 250px; white-space: normal;">
                        <div class="text-pre-wrap"><q-input type="text" v-model="records.data[props.row.id].comment" dense autofocus emit-value /></div>
                    </q-td>
                    <q-td key="time" :props="props" style="width: 150px;">
                              <div class="q-pa-md" >
                                <div style="width: 40%; float:left; margin-right: 5px;">
                                    <q-input type="number" v-model.number="records.data[props.row.id].timeHours" dense autofocus min="0" hint="Hours" emit-value />
                                </div>
                                <div style="width: 40%; float: right; margin-right: 5px;">
                                    <q-input type="number" v-model.number="records.data[props.row.id].timeMinutes" dense autofocus min="0" max="59" hint="Minutes" emit-value />
                                </div>
                            </div>
                    </q-td>
                    <q-td key="option" :props="props" style="max-width: 30px">
                        <template v-if="props.row.task > 0 && options.data">
                            <template  v-if="options.data[props.row.task]">
                                <q-btn unelevated rounded size="9px" text-color="white" :loading="props.row.saveLoading" label="save" :key="action" style="margin-right: 5px; background:#3bc8f5;" @click.stop="action.actions.save(props.row)"/>
                            </template>
                        </template>
                        <q-btn unelevated rounded size="9px" text-color="white" :loading="props.row.dismissLoading" label="dismiss" :key="action" style="margin-right: 5px; background:#3bc8f5;" @click.stop="action.actions.dismiss(props.row)"/>
                    </q-td>
                </q-tr>
            </template>
        </q-table>
    </template>
    <br/>
    <q-separator color="grey" inset />
    </div>`;

const app = createApp({
    name: "Timetracker",
    template,
    setup(){
        const pagination = {
            rowsPerPage: 0,
            sortBy: 'id',
            descending: true
        }

        const taskURL = "https://portal.intreface.com/company/personal/user/"

        const taskLink = computed(() => {
            let user = BX.message("USER_ID")
            return taskURL + user + "/tasks/task/view/"

        })

        const columns = [
            { name: 'id', align: 'left', label: 'ID', field: 'id', sortable: false, sort: (a, b) => a - b },
            { name: 'name', align: 'left', label: 'Name', field: 'name' },
            { name: 'type', align: 'center', label: 'Type', field: 'type' },
            { name: 'task', align: 'center', label: 'Task', field: 'task' },
            { name: 'total', label: 'Total Logged', field: 'totalHours & totalMinutes', align: 'center' },
            { name: 'comment', label: 'Comment', field: 'comment', align: 'center' },
            { name: 'time', label: 'Log Time', field: 'timeHours & timeMinutes', align: 'center' },
            { name: 'option', label: 'Options', align: 'center'}
        ]

        const records = reactive({data: []})

        const getRecords = computed(() => {
            let actions = [];
            Object.values(records.data).forEach(y => {
                actions.push({...y})
            })
            return actions
        })

        const logged = reactive({data: 0})

        const totalLogged = computed(() => {
            let hours = logged.data[0] !== undefined ? logged.data[0] : 0
            let minutes = logged.data[1] !== undefined ? logged.data[1] : 0
            return hours + ' Hours and ' + minutes + ' Minutes'
        })

        const dailyActions = computed(() => {
            let count = 0;
            Object.values(records.data).forEach(y => {
                if(y.id > 0){
                    count++
                }
            })
            return count
        })

        const options = reactive({data: [], dataFiltered: []})

        const getOptions = computed(() => {
            let tasks = [];
            Object.values(options.dataFiltered).forEach(x => {
                tasks.push({...x})
            })
            return tasks
        })

        let loading = ref(false)

        let view = ref(false)

        let internalCallLoading = ref(false)

        let externalCallLoading = ref(false)

        const action = {
            actions: {
                save(row) {
                    loading.value = true
                    row.saveLoading = true
                    let seconds = action.actions.convertToSeconds(row.timeHours, row.timeMinutes)
                    if(seconds > 240) {
                        BX.ajax.runAction('intreface:module.timetracker.Tracker.save', { data: {
                                actionId:row.id,
                                taskLogId:row.task,
                                seconds: seconds,
                                comment: row.comment,
                                userId: BX.message("USER_ID") }
                        }).then(response => {
                            action.actions.calculateTotal();
                            delete records.data[row.id]
                            Notify.create({
                                group: false,
                                message: 'Saved',
                                color: 'green'
                            })
                            loading.value = false
                            row.saveLoading = false
                        }).catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not save action ID:'+row.id,
                                color: 'red'
                            })
                            row.saveLoading = false
                            loading.value = false
                        })
                    }else{
                        Notify.create({
                            group: false,
                            message: 'Error',
                            caption: 'Could not save value for action ID:'+row.id + ' entered values under 5 minutes',
                            color: 'red'
                        })
                        row.saveLoading = false
                        loading.value = false
                    }
                },

                dismiss(row){
                    loading.value = true
                    row.dismissLoading = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.dismiss', { data: { id:row.id } })
                        .then(response => {
                            delete records.data[row.id]
                            Notify.create({
                                group: false,
                                message: 'Dismissed',
                                color: 'green'
                            })
                            loading.value = false
                            row.dismissLoading = false
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not dismiss row ID:'+row.id + ' - ' + e[1],
                                color: 'red'
                            })
                            loading.value = false
                            row.dismissLoading = false
                        })
                },

                updateUserActions(){
                    loading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getUserDailyActionList', { data: { userId: BX.message("USER_ID") } })
                        .then(response => {
                            response.data.forEach(record => {
                                let totalFormattedTime = action.actions.convertFromSeconds(record['TOTAL_LOGGED'])
                                if(records.data[record.ID]){
                                    records.data[record.ID]["id"] = record.ID
                                    records.data[record.ID]["name"] = record["NAME"]
                                    records.data[record.ID]["task"] = record["TASK_LOG_ID"] > 0 ? record["TASK_LOG_ID"] : null
                                    records.data[record.ID]["type"] = record["ORIGIN_TYPE"]
                                    records.data[record.ID]["totalHours"] = totalFormattedTime[0]
                                    records.data[record.ID]["totalMinutes"] = totalFormattedTime[1]
                                    records.data[record.ID]["comment"] = ''
                                    records.data[record.ID]["timeHours"] = records.data[record.ID]["timeHours"] > 0 ? records.data[record.ID]["timeHours"] : 0
                                    records.data[record.ID]["timeMinutes"] = records.data[record.ID]["timeMinutes"] > 0 ? records.data[record.ID]["timeMinutes"] : 0
                                }else{
                                    records.data[record.ID] = {
                                        id: record.ID,
                                        name: record["NAME"],
                                        task: record["TASK_LOG_ID"] > 0 ? record["TASK_LOG_ID"] : null,
                                        type: record["ORIGIN_TYPE"],
                                        totalHours: totalFormattedTime[0],
                                        totalMinutes: totalFormattedTime[1],
                                        comment: '',
                                        timeHours: 0,
                                        timeMinutes: 0,
                                        saveLoading: false,
                                        dismissLoading: false
                                    }
                                }
                            })
                            loading.value = false
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not get Actions - ' + e[1],
                                color: 'red'
                            })
                            loading.value = false
                        })
                },

                getUserActions() {
                    loading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getUserDailyActionList', { data: { userId: BX.message("USER_ID") } })
                        .then(response => {
                            response.data.forEach(record => {
                                let totalFormattedTime = action.actions.convertFromSeconds(record['TOTAL_LOGGED'])
                                records.data[record.ID] = {
                                    id: record.ID,
                                    name: record["NAME"],
                                    task: record["TASK_LOG_ID"] > 0 ? record["TASK_LOG_ID"] : null,
                                    type: record["ORIGIN_TYPE"],
                                    totalHours: totalFormattedTime[0],
                                    totalMinutes: totalFormattedTime[1],
                                    comment: '',
                                    timeHours: 0,
                                    timeMinutes: 0,
                                    saveLoading: false,
                                    dismissLoading: false
                                }
                            })
                            loading.value = false
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not get Actions - ' + e[1],
                                color: 'red'
                            })
                        })
                },

                calculateTotal(){
                    let totalLogged = 0;
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getFullUserDailyActionList', { data: { userId: BX.message("USER_ID") } })
                        .then(response => {
                            response.data.forEach(record => {
                                totalLogged = parseFloat(totalLogged) + parseFloat(record['TOTAL_LOGGED'])
                            })
                            logged.data = action.actions.convertFromSeconds(totalLogged)
                            loading.value = false
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not Total Time Logged',
                                color: 'red'
                            })
                        })
                },

                convertFromSeconds(seconds){
                    let hours = Math.floor(seconds / 60 / 60);
                    let minutes = Math.floor((seconds / 60) - (hours * 60))
                    return [hours, minutes]
                },

                convertToSeconds(hours, minutes){
                    let seconds = (hours * 60 * 60) + (minutes * 60);
                    return seconds
                },

                addIntCall() {
                    loading.value = true
                    externalCallLoading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.addInternalCall', {
                        data: { originId: 0, userId: BX.message("USER_ID") }
                    }).then(response => {
                        loading.value = false
                        externalCallLoading.value = false
                        Notify.create({
                            group: false,
                            message: 'Success',
                            caption: 'New Internal Call Added' ,
                            color: 'green'
                        })
                        action.actions.updateUserActions()
                    })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could add new call - ' + e[1],
                                color: 'red'
                            })
                            externalCallLoading.value = false
                            loading.value = false
                        })
                },

                addExtCall() {
                    loading.value = true
                    internalCallLoading.value = true
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.addExternalCall', {
                        data: { originId: 0, userId: BX.message("USER_ID") }
                    }).then(response => {
                        loading.value = false
                        internalCallLoading.value = false
                        Notify.create({
                            group: false,
                            message: 'Success',
                            caption: 'New External Call Added' ,
                            color: 'green'
                        })
                        action.actions.updateUserActions()
                    })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could add new call - ' + e[1],
                                color: 'red'
                            })
                            internalCallLoading.value = false
                            loading.value = false
                        })
                }
            },

            table: {
                toggle(){
                    view.value = !view.value
                }
            },

            tasks: {
                getTaskData() {
                    BX.ajax.runAction('intreface:module.timetracker.Tracker.getTaskList')
                        .then(response => {
                            response.data.forEach(record => {
                                options.data[record.value] = record
                            });
                        })
                        .catch(e => {
                            Notify.create({
                                group: false,
                                message: 'Error',
                                caption: 'Could not load tasks',
                                color: 'red'
                            })
                        })
                },

                filterFn (val, update, abort) {
                    setTimeout(() => {
                        update(
                            () => {
                                if (val === '') {
                                    options.dataFiltered = options.data
                                } else {
                                    const needle = val.toLowerCase()
                                    options.dataFiltered = options.data.filter(v => v.label.toLowerCase().indexOf(needle) > -1)
                                }
                            }
                        )
                    }, 300)
                }
            }
        }

        onBeforeMount(() => {
            action.tasks.getTaskData();
            action.actions.getUserActions();
            action.actions.calculateTotal();
        });


        return{
            records,
            columns,
            options,
            pagination,
            view,
            action,
            loading,
            getRecords,
            taskURL,
            taskLink,
            dailyActions,
            getOptions,
            totalLogged
        }
    }
});

app.use(Quasar, {
    plugins: { Notify }
});
app.mount("#timeTracker");




