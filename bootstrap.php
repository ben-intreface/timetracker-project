<?php

/*
 * Author Ben Berhanu
 * With contribution from Mikkie
 */


declare(strict_types=1);

use \Bitrix\Main\{
    IO,
    Event,
    EventManager,
    Application,
    Type,
    IO\File,
    Engine\Controller
};

use \Bitrix\{
    Main
};

use \Intreface\Helper\{
    Loader,
    Main\MonkeyPatch,
    Main\Asset
};

use \Intreface\Module\{
    TimeTracker\Controller\Tracker,
    TimeTracker\Action,
    TimeTracker\Tasks,
    TimeTracker\ActionLogItem
};


Loader::include(new IO\Directory(__DIR__ . '/events/'));
Loader::include(new IO\Directory(__DIR__ . '/patches/'));

// Register controller folder
Loader::registerControllerNamespace('intreface.module', [
    '\\Intreface\\Module\\TimeTracker\\Controller' => 'timetracker',
]);



/*
 * Disabled this event for now. Some participants have felt this is too excessive
 */
//EventManager::getInstance()->addEventHandler(
//    "tasks",
//    'onTaskUpdateViewed',
//    function (Event $event) {
//        /*
//         * When a user views a task send request to the tracker to add new action.
//         */
//        return Tracker::addTaskAction($event->getParameter("taskId"), $event->getParameter("userId"));
//
//    }
//);

EventManager::getInstance()->addEventHandler(
    "tasks",
    '\\Bitrix\\Tasks\\Internals\\Task::onAfterUpdate',

    function (Event $event) {

        /*
         * When a user updates a task send request to the tracker to add new action.
         */
        $id = $event->getParameter("id");
        Tracker::addTaskAction((int)$id["ID"], (int)$_SESSION["SESS_AUTH"]["USER_ID"]);
    }
);

EventManager::getInstance()->addEventHandler(
    "tasks",
    'onTaskUpdate',
    function ($id, $fields) {
        /*
         * When a user updates a task send request to the tracker to add new action.
        */
        Tracker::addTaskAction((int)$id, (int)$_SESSION["SESS_AUTH"]["USER_ID"]);

    }
);

EventManager::getInstance()->addEventHandler(
    'forum',
    'OnCommentsInit',
    function () {
        /*
         * When a user comments on a task send request to the tracker to add new action.
         */
        if($_REQUEST["ENTITY_ID"] && $_REQUEST["ENTITY_TYPE"] === "TK") {
            return Tracker::addTaskAction((int)$_REQUEST["ENTITY_ID"], (int)$_SESSION["SESS_AUTH"]["USER_ID"]);
        }

    }
);

EventManager::getInstance()->addEventHandler(
    "tasks",
    'onTaskAdd',
    function ($id, $fields) {
        /*
         * When a user adds a new task send request to the tracker to add new action.
         */
        Tracker::addTaskAction((int)$id, (int)$_SESSION["SESS_AUTH"]["USER_ID"]);

    }
);

EventManager::getInstance()->addEventHandler(
    "mail",
    'onMailMessageNew',
    function (Event $event) {
        /*
         * When a new email is recieved send request to the tracker to add new action.
         */

        $message = $event->getParameter("message");
        $emailId = $message["ID"];

        Tracker::addEmailAction((int) $emailId, (int) $event->getParameter("userId"));

    }
);

EventManager::getInstance()->addEventHandler(
    "support",
    'OnAfterTicketUpdate',
        function () {
            /*
             * When a new email a ticket is updated send request to the tracker to add new action.
             */
            Tracker::addTicketAction((int) $_REQUEST["ID"], (int)$_SESSION["SESS_AUTH"]["USER_ID"]);

        }
);

EventManager::getInstance()->addEventHandler(
    "support",
    'OnAfterTicketAdd',
    function (){
        /*
         * When a ticket is answered send request to the tracker to add new action.
         */
        return Tracker::addTicketAction((int) $_REQUEST["ID"], (int)$_SESSION["SESS_AUTH"]["USER_ID"]);

    }
);

EventManager::getInstance()->addEventHandler(
    "intreface.module-timetracker",
    '\\Intreface\\Module\\TimeTracker\\Action::onBeforeAddTime',
    function (Event $event) {
        /*
        * Before Adding time to the action
        * Add user's logged time against the task
        */

        $currentTime = new Type\DateTime();

        // Get the task
        $task = (new Tasks($event->getParameter("taskLogId")));

        // Build request using values from the the event
        $fields = [
            "CREATED_DATE" => $currentTime,
            "DATE_START" => $currentTime,
            "DATE_STOP" => $currentTime,
            "USER_ID" => $event->getParameter("userId"),
            "SECONDS" => $event->getParameter("input"),
            "COMMENT_TEXT" => $event->getParameter("comment"),
            "SOURCE" => 2 // Manual
        ];

        // execute request to add user's inputted time
        $task->addElapsedTime((array)$fields, (int) $event->getParameter("taskLogId"), (int) $event->getParameter("userId"));

        return true;
    }
);

EventManager::getInstance()->addEventHandler(
    "intreface.module-timetracker",
    '\\Intreface\\Module\\TimeTracker\\Action::onAfterAddTime',
    function (Event $event) {

        /*
        * After an action is unlocked add this event to the log.
        */
        $action = new Action($event->getParameter("ACTION_ID"));

        // Build request using values from the action
        $data = [
            "ACTION_ID" => $action->actionId(),
            "NAME" => $action->name(),
            "ORIGIN_ID" => $action->originId(),
            "ORIGIN_TYPE" => $action->originType(),
            "TASK_LOG_ID" => $action->loggedId(),
            "LOGGED_TIME" => $action->loggedTime(),
            "TOTAL_LOGGED" => $action->totalLogged(),
            "ACTION_DATE_CREATED" => $action->dateCreate(),
            "USER_ID" => $action->userId(),
            ];

        // create new log item
        $result = ActionLogItem::create((array) $data);

        if(!$result){
            return false;
        }

        return true;
    }
);

EventManager::getInstance()->addEventHandler(
    "intreface.module-timetracker",
    '\\Intreface\\Module\\TimeTracker\\Action::onAfterCreate',

    function (Event $event) {

        /*
         * After a new action is created add this event to the log.
         */
        $action = new Action($event->getParameter("ACTION_ID"));

        // Build request using values from the action
        $data = [
            "ACTION_ID" => $action->actionId(),
            "NAME" => $action->name(),
            "ORIGIN_ID" => $action->originId(),
            "ORIGIN_TYPE" => $action->originType(),
            "TASK_LOG_ID" => $action->loggedId(),
            "LOGGED_TIME" => 0,
            "TOTAL_LOGGED" => $action->totalLogged(),
            "ACTION_DATE_CREATED" => $action->dateCreate(),
            "USER_ID" => $action->userId(),

        ];

        // create new log item
        $result = ActionLogItem::create((array)$data);

        if(!$result){
            return false;
        }

        return true;
    }
);

EventManager::getInstance()->addEventHandler(
    "intreface.module-timetracker",
    '\\Intreface\\Module\\TimeTracker\\Action::onAfterUnlock',

    function (Event $event) {

        /*
        * After an action is unlocked add this event to the log.
        */
        $action = new Action($event->getParameter("ACTION_ID"));

        // Build request using values from the action
        $data = [
            "ACTION_ID" => $action->actionId(),
            "NAME" => $action->name(),
            "ORIGIN_ID" => $action->originId(),
            "ORIGIN_TYPE" => $action->originType(),
            "TASK_LOG_ID" => $action->loggedId(),
            "LOGGED_TIME" => 0,
            "TOTAL_LOGGED" => $action->totalLogged(),
            "ACTION_DATE_CREATED" => $action->dateCreate(),
            "USER_ID" => $action->userId()
        ];

        // create new log item
        $result = ActionLogItem::create((array)$data);

        return $result > 0;

    }
);
