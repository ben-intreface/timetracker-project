<?php

use \Intreface\Module\{
    AdminMenu,
    TimeTracker\Controller\Tracker,
    TimeTracker\Internals\ActionTimeTable,
    TimeTracker\Action,
    TimeTracker\Tasks,
    TimeTracker\Emails,
    TimeTracker\Settings
};

// If action locked succefully function will return true
$resolvedEmail = Emails::resolveEmailAddress(["Akiva Shtisel <cool@mail.com>", "Ben Berhanu <ben@intreface.com>"]);
echo print_r(["resolveEmailAddress - Outcome 1 - Success" => ["RESULT" => $resolvedEmail == "Akiva Shtisel <cool@mail.com>" ? "Pass" : "Fail", $resolvedEmail, "EXPECTED_RESULT" => "Pass"]], true);

$resolvedEmail = Emails::resolveEmailAddress(["ben@intreface.com", "test@test.com"]);
echo print_r(["resolveEmailAddress - Outcome 2 - Success" => ["RESULT" => $resolvedEmail == "test@test.com" ? "Pass" : "Fail", $resolvedEmail, "EXPECTED_RESULT" => "Pass"]], true);

$resolvedEmail = Emails::resolveEmailAddress(["info@intreface.com", "ben@intreface.com"]);
echo print_r(["resolveEmailAddress - Outcome 3 - Success" => ["RESULT" => $resolvedEmail == Settings::INTERNAL_EMAIL_PLACEHOLDER ? "Pass" : "Fail", $resolvedEmail, "EXPECTED_RESULT" => "Pass"]], true);

$resolvedEmail = Emails::resolveEmailAddress(["Akiva Shtisel <cool@mail.com>", "Ben Berhanu <ben@intreface.com>"]);
echo print_r(["resolveEmailAddress - Outcome 4 - Fail" => ["RESULT" => $resolvedEmail !== "Akiva Shtisel <cool@mail.com>" ? "Pass" : "Fail", $resolvedEmail, "EXPECTED_RESULT" => "Fail"]], true);

$resolvedEmail = Emails::resolveEmailAddress(["ben@intreface.com", "test@test.com"]);
echo print_r(["resolveEmailAddress - Outcome 5 - Fail" => ["RESULT" => $resolvedEmail !== "test@test.com" ? "Pass" : "Fail", $resolvedEmail, "EXPECTED_RESULT" => "Fail"]], true);

$resolvedEmail = Emails::resolveEmailAddress(["info@intreface.com", "ben@intreface.com"]);
echo print_r(["resolveEmailAddress - Outcome 6 - Fail" => ["RESULT" => $resolvedEmail !== Settings::INTERNAL_EMAIL_PLACEHOLDER ? "Pass" : "Fail", $resolvedEmail, "EXPECTED_RESULT" => "Fail"]], true);

// Test Action Chain in ID > 0 test returned action ID - pass, existing chain
$actionChain = Emails::guessActionChain("Akiva Shtisel <cool@mail.com>, Shulem Shtisel <mack@mail.com>", "This is a New Email", 1);
echo print_r(["guessActionChain - Success" => ["RESULT" => $actionChain > 0 ? "Pass" : "Fail", $actionChain, "EXPECTED_RESULT" => "Pass"]], true);

// test should fail - no existing chain value should be 0
$actionChain = Emails::guessActionChain("test@test.fail", "This is a fail test", 1);
echo print_r(["guessActionChain - Fail" => ["RESULT" => $actionChain > 0 ? "Pass" : "Fail", $actionChain, "EXPECTED_RESULT" => "Fail"]], true);
