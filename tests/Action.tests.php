<?php

use \Intreface\Module\{
    AdminMenu,
    TimeTracker\Controller\Tracker,
    TimeTracker\Internals\ActionTimeTable,
    TimeTracker\Action,
    TimeTracker\Tasks
};
use \Bitrix\Main\{
    Type
};

// If action locked succefully function will return true
$lockAction = new Action(93);
echo print_r(["lock - Success" => ["RESULT" => $lockAction->lock() ? "Pass" : "Fail", $lockAction->lock(), "EXPECTED_RESULT" => "Pass"]], true);

// If action unlocked succefully function will return true
$unlockAction = new Action(93);
echo print_r(["unlock - Success" => ["RESULT" => $unlockAction->unlock() ? "Pass" : "Fail", $unlockAction->unlock(), "EXPECTED_RESULT" => "Pass"]], true);

// If action is mine return true
$myAction = new Action(3);
echo print_r(["isMine - Success" => ["RESULT" => $myAction->isMine() ? "Pass" : "Fail", $myAction->isMine(), "EXPECTED_RESULT" => "Pass"]], true);

// this test should fail - action not for this user
$noMyAction = new Action(93);
echo print_r(["isMine - Fail" => ["RESULT" => $noMyAction->isMine() ? "Pass" : "Fail", $noMyAction->isMine(), "EXPECTED_RESULT" => "Fail"]], true);

// check if action exists for the day
$checkAction = Action::checkDailyActionExists(1, "TASK", 20);
echo print_r(["checkDailyActionExists - Success" => ["RESULT" => $checkAction > 0 ? "Pass" : "Fail", $checkAction, "EXPECTED_RESULT" => "Pass"]], true);

// this test should fail -  check if action exists for the day - action does not exist
$failCheckAction = Action::checkDailyActionExists(3, "TASK", 24);
echo print_r(["checkDailyActionExists - Fail" => ["RESULT" => $failCheckAction > 0 ? "Pass" : "Fail", $failCheckAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Task action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$taskAction = Action::createTaskAction(20, 1);
echo print_r(["createTaskAction - Success" => ["RESULT" => $taskAction > 0 ? "Pass" : "Fail", $taskAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$failTaskAction = Action::createTaskAction(20, 4);
echo print_r(["createTaskAction - Fail" => ["RESULT" => $failTaskAction > 0 ? "Pass" : "Fail", $failTaskAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$ticketAction = Action::createTicketAction(51, 1);
echo print_r(["createTicketAction - Success" => ["RESULT" => $ticketAction > 0 ? "Pass" : "Fail", $ticketAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$failTicketAction = Action::createTicketAction(51, 4);
echo print_r(["createTicketAction - Fail" => ["RESULT" => $failTicketAction > 0 ? "Pass" : "Fail", $failTicketAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Email action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$emailAction = Tracker::addEmailAction(6, 1);
echo print_r(["addEmailAction - Success" => ["RESULT" => $emailAction > 0 ? "Pass" : "Fail", $emailAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$failEmailAction = Tracker::addEmailAction(6, 4);
echo print_r(["addEmailAction - Fail" => ["RESULT" => $failEmailAction > 0 ? "Pass" : "Fail", $failEmailAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Internal Call action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$internalCallAction = Action::createInternalCallAction(1);
echo print_r(["createInternalCallAction - Success" => ["RESULT" => $internalCallAction > 0 ? "Pass" : "Fail", $internalCallAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$failInternalCallAction = Action::createInternalCallAction(4);
echo print_r(["createInternalCallAction - Fail" => ["RESULT" => $failInternalCallAction > 0 ? "Pass" : "Fail", $failInternalCallAction, "EXPECTED_RESULT" => "Fail"]], true);

// If External Call action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$externalCallAction = Action::createExternalCallAction(1);
echo print_r(["createExternalCallAction - Success" => ["RESULT" => $externalCallAction > 0 ? "Pass" : "Fail", $externalCallAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$failExternalCallAction = Action::createExternalCallAction(4);
echo print_r(["createExternalCallAction - Fail" => ["RESULT" => $failExternalCallAction > 0 ? "Pass" : "Fail", $failExternalCallAction, "EXPECTED_RESULT" => "Fail"]], true);

