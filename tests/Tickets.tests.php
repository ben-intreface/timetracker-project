<?php

use \Intreface\Module\{
    AdminMenu,
    TimeTracker\Controller\Tracker,
    TimeTracker\Internals\ActionTimeTable,
    TimeTracker\Action,
    TimeTracker\Tasks,
    TimeTracker\Emails,
    TimeTracker\Settings,
    TimeTracker\Tickets
};
use \Bitrix\Main\{
    Type
};

// If internal task returned successfully function will return the task ID so result should be greater than 0
$ticket = Tickets::getTicket(51);
echo print_r(["getTicket - Success" => ["RESULT" => !empty($ticket) ? "Pass" : "Fail", $ticket["ID"], "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, ticket does not exist
$emptyTicket = Tickets::getTicket(100);
echo print_r(["getTicket - Fail" => ["RESULT" => !empty($emptyTicket) ? "Pass" : "Fail", $emptyTicket, "EXPECTED_RESULT" => "Fail"]], true);

// If internal task returned successfully function will return the task ID so result should be greater than 0
$ticketName = Tickets::getTicketName(51);
echo print_r(["getTicketName - Success" => ["RESULT" => is_string($ticketName) && $ticketName == "Yello" ? "Pass" : "Fail", $ticketName, "EXPECTED_RESULT" => "Pass"]], true);

// If ticket not found will return empty string
$ticketName = Tickets::getTicketName(100);
echo print_r(["getTicketName - alt outcome - Success" => ["RESULT" => is_string($ticketName) && $ticketName == "" ? "Pass" : "Fail", $ticketName, "EXPECTED_RESULT" => "Pass"]], true);

// If ticket found succesfully return array with ticket auther details
$userFromTicket = Tickets::getUserFromTicket($ticket);
echo print_r(["getUserFromTicket - Success" => ["RESULT" => !empty($userFromTicket) ? "Pass" : "Fail", $userFromTicket["ID"] , "EXPECTED_RESULT" => "Pass"]], true);

// If Ticket empty user array returned as empty
$emptyUserFromTicket  = Tickets::getUserFromTicket($emptyTicket);
echo print_r(["getUserFromTicket - Fail" => ["RESULT" => !empty($emptyUserFromTicket) ? "Pass" : "Fail", $emptyUserFromTicket, "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket empty user array returned as empty
$projectFromUser  = Tickets::getUserProject($userFromTicket);
echo print_r(["getUserProject - Success" => ["RESULT" => !empty($projectFromUser) ? "Pass" : "Fail", $projectFromUser, "EXPECTED_RESULT" => "Pass"]], true);

// If Ticket empty user array returned as empty
$emptyProjectFromUser  = Tickets::getUserProject($emptyUserFromTicket);
echo print_r(["getUserProject - Fail" => ["RESULT" => !empty($emptyProjectFromUser) ? "Pass" : "Fail", $emptyProjectFromUser, "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket empty user array returned as empty
$supportTask  = Tickets::getSupportTask($projectFromUser);
echo print_r(["getSupportTask - Success" => ["RESULT" => !empty($supportTask) ? "Pass" : "Fail", $projectFromUser, "EXPECTED_RESULT" => "Pass"]], true);

// If Ticket empty user array returned as empty
$emptySupportTask  = Tickets::getSupportTask($emptyProjectFromUser);
echo print_r(["getSupportTask - Fail" => ["RESULT" => !empty($emptySupportTask) ? "Pass" : "Fail", $emptyProjectFromUser, "EXPECTED_RESULT" => "Fail"]], true);
