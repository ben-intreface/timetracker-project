<?php

use \Intreface\Module\{
    AdminMenu,
    TimeTracker\Controller\Tracker,
    TimeTracker\Internals\ActionTimeTable,
    TimeTracker\Action,
    TimeTracker\Tasks
};


// If Email action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$emailAction = Tracker::addEmailAction(6, 1);
echo print_r(["addEmailAction - Pass" => ["RESULT" => $emailAction > 0 ? "Pass" : "Fail", $emailAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$emailAction = Tracker::addEmailAction(6, 4);
echo print_r(["addEmailAction - Fail" => ["RESULT" => $emailAction > 0 ? "Pass" : "Fail", $emailAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$ticketAction = Tracker::addTicketAction(51, 1);
echo print_r(["addTicketAction - Pass" => ["RESULT" => $ticketAction > 0 ? "Pass" : "Fail", $ticketAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$ticketAction = Tracker::addTicketAction(51, 4);
echo print_r(["addTicketAction - Fail" => ["RESULT" => $ticketAction > 0 ? "Pass" : "Fail", $ticketAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Task action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$taskAction = Tracker::addTaskAction(20, 1);
echo print_r(["addTaskAction - Pass" => ["RESULT" => $taskAction > 0 ? "Pass" : "Fail", $taskAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$taskAction = Tracker::addTaskAction(20, 4);
echo print_r(["addTaskAction - Fail" => ["RESULT" => $taskAction > 0 ? "Pass" : "Fail", $taskAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$internalCallAction = Tracker::addInternalCallAction(1);
echo print_r(["addInternalCallAction - Pass" => ["RESULT" => $internalCallAction > 0 ? "Pass" : "Fail", $internalCallAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$internalCallAction = Tracker::addInternalCallAction(4);
echo print_r(["addInternalCallAction - Fail" => ["RESULT" => $internalCallAction > 0 ? "Pass" : "Fail", $internalCallAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket action added succefully function will return the action ID if value is greater than 0 then action added succesfully
$externalCallAction = Tracker::addExternalCallAction(1);
echo print_r(["addExternalCallAction - Pass" => ["RESULT" => $externalCallAction > 0 ? "Pass" : "Fail", $externalCallAction, "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, user not in timetracker group
$externalCallAction = Tracker::addExternalCallAction(4);
echo print_r(["addExternalCallAction - Fail" => ["RESULT" => $externalCallAction > 0 ? "Pass" : "Fail", $externalCallAction, "EXPECTED_RESULT" => "Fail"]], true);

// If Task action saved succefully function will return true value else false
$saveAction = Tracker::saveAction(62, 22, 1000, "Task comment", 1);
$saveValue = new Action(62);
echo print_r(["saveAction - Pass" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, action not for user
$saveAction = Tracker::saveAction(88, 20, 1000, "Task comment", 1);
$saveValue = new Action(62);
echo print_r(["saveAction - Fail" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Fail"]], true);

// If Ticket action saved succefully function will return true value else false
$saveAction = Tracker::saveAction(71, 24, 1000, "Ticket comment", 1);
$saveValue = new Action(62);
echo print_r(["saveAction - Pass" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, action not for user
$saveAction = Tracker::saveAction(89, 24, 1000, "Ticket comment", 1);
$saveValue = new Action(62);
echo print_r(["saveAction - Fail" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Fail"]], true);

// If Email action saved succefully function will return true value else false
$saveAction = Tracker::saveAction(39, 24, 1000, "Email comment", 1);
$saveValue = new Action(39);
echo print_r(["saveAction - Pass" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, action not for user
$saveAction = Tracker::saveAction(93, 24, 1000, "Email comment", 1);
$saveValue = new Action(93);
echo print_r(["saveAction - Fail" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Fail"]], true);

// If Internal Call action saved succefully function will return true value else false
$saveAction = Tracker::saveAction(40, 11, 1000, "IC comment", 1);
$saveValue = new Action(40);
echo print_r(["saveAction - Pass" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, action not for user
$saveAction = Tracker::saveAction(91, 30, 1000, "IC comment", 1);
$saveValue = new Action(91);
echo print_r(["saveAction - Fail" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Fail"]], true);

// If External Call action saved succefully function will return true value else false
$saveAction = Tracker::saveAction(41, 24, 1000, "EC comment", 1);
$saveValue = new Action(41);
echo print_r(["saveAction - Pass" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, action not for user
$saveAction = Tracker::saveAction(92, 24, 1000, "EC comment", 1);
$saveValue = new Action(92);
echo print_r(["saveAction - Fail" => ["RESULT" => $saveAction ? "Pass" : "Fail", ["LAST_LOGGED" => $saveValue->loggedTime(), "TASK_LOGGED_ID" => $saveValue->loggedId()], "EXPECTED_RESULT" => "Fail"]], true);

// If action dismissed succefully function will return true value else false
$saveAction = Tracker::dismissAction(42);
echo print_r(["dismissAction - Pass" => ["RESULT" => $saveAction ? "Pass" : "Fail" , "EXPECTED_RESULT" => "Pass"]], true);

// Test should fail, action not for user
$saveAction = Tracker::dismissAction(88);
echo print_r(["dismissAction - Fail" => ["RESULT" => $saveAction ? "Pass" : "Fail", "EXPECTED_RESULT" => "Fail"]], true);
