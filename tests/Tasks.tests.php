<?php

use \Intreface\Module\{
    AdminMenu,
    TimeTracker\Controller\Tracker,
    TimeTracker\Internals\ActionTimeTable,
    TimeTracker\Action,
    TimeTracker\Tasks,
    TimeTracker\Emails,
    TimeTracker\Settings
};
use \Bitrix\Main\{
    Type
};

// If time logged succesfully return true
$currentTime = new Type\DateTime();
$task = (new Tasks(24));
$fields = [
    "CREATED_DATE" => $currentTime,
    "DATE_START" => $currentTime,
    "DATE_STOP" => $currentTime,
    "USER_ID" => 1,
    "SECONDS" => 1000,
    "COMMENT_TEXT" => "Test Comment",
    "SOURCE" => 2 // Manual
];
$addedTime = $task->addElapsedTime((array)$fields, (int)24, (int)1);
echo print_r(["addElapsedTime - Pass" => ["RESULT" => $addedTime ? "Pass" : "Fail", $addedTime, "EXPECTED_RESULT" => "Pass"]], true);

// If internal task returned successfully function will return the task ID so result should be greater than 0
$userInternalTask = Tasks::getInternalTaskId(1);
echo print_r(["getInternalTaskId - Pass" => ["RESULT" => $userInternalTask > 0 ? "Pass" : "Fail", $userInternalTask, "EXPECTED_RESULT" => "Pass"]], true);



